import TelegramBot from 'node-telegram-bot-api'
import fs from 'fs'
import JsonDB from 'node-json-db'
import _ from 'lodash'

const token = '474400789:AAHSv3SNECu7DCjnXy-_QYcbDGH3PNUvQYM'
const bot = new TelegramBot(token, { polling: true })


var db =  new JsonDB('tasksDao', true, false);
var data = db.getData('/')


bot.onText(/\/help (.+)/, (msg, [source, match])=>{
    const { chat: { id }} = msg
    bot.sendMessage(id, match)
});

bot.onText(/\/list/,(msg) => {
    var stringall = _.map(data, (value,i)=>{
            return "\n" + i + " : " + value.task + (value.completed?' ->выполнено':' ->невыполнено');
    })
    const { chat: { id }} = msg
    bot.sendMessage(id, "Список всех задач" +  _.toString(stringall))
});


bot.onText(/\/add_task (.+)/,(msg, [source, match]) => {
    var size = Object.keys(data).length;
    db.push("/" + ++size, {task:match, completed:false}, true);
    const { chat: { id }} = msg
    bot.sendMessage(id, 'Задача добавлена')
});

bot.onText(/\/complete ([0-9])/,(msg, [source, match]) => {
    const { chat: { id }} = msg
    try{
        var oneTask = db.getData("/" + match)
        db.push("/" + match, {task: oneTask.task, completed:!oneTask.completed}, true)
    }catch(error){
        bot.sendMessage(id, 'Такой записи не существует, попробуйте снова' + error)
    }
    bot.sendMessage(id, 'Статус задачи изменён')
});


